require 'sinatra'
configure :production do
  require 'newrelic_rpm'
end

set :public_folder, File.dirname(__FILE__) + '/assets'

get '/' do
  app_name = "KeepingTracks"
  app_about = "Keep track of your rail journeys in the UK, with your Android device."
  app_subhead = "is an Android app to help you keep track of your rail journeys."
  homepage_path = "https://bitbucket.org/sea_wolf/keepingtracks"
  trello_url = "https://trello.com/b/0xS1MOQd"
  github_path = "https://github.com/seawolf/KeepingTracks"
  download_path = github_path + "/releases"
  issues_path = github_path + "/issues"
  fdroid_path = "http://f-droid.org"
  fdroid_app_path = fdroid_path + "/repository/browse/?fdid=com.seawolfsanctuary.keepingtracks"
  fdroid_client_path = ""
  irc_path = "http://webchat.freenode.net/?channels=keepingtracks"
  erb :index, :locals => {
    :app_name => app_name,
    :app_about => app_about,
    :app_subhead => app_subhead,
    :homepage_path => homepage_path,
    :trello_url => trello_url,
    :download_path => download_path,
    :issues_path => issues_path,
    :fdroid_path => fdroid_path,
    :fdroid_app_path => fdroid_app_path,
    :fdroid_client_path => fdroid_client_path,
    :irc_path => irc_path
  }
end

get '/status' do
  Time.now.to_s
end

get '/class_photos/thumbs/:id' do
  id = params['id'].to_s.match(/^[0-9\-]*$/)
  filename = 'class_photos/thumbs/${id}'
  if File.exists?(filename)
    send_file(filename)
  else
    return 404
  end
end

get '/class_photos/:id' do
  id = params['id'].to_s.match(/^[0-9\-]*$/)
  filename = 'class_photos/${id}'
  if File.exists?(filename)
    send_file(filename)
  else
    return 404
  end
end

get '/app/foursquare/client_id' do "ZPFECAOBGGX5YDDTWSIV3OZ1525OQ3JIRLALI4EXQR5MVCML" end
get '/app/foursquare/client_secret' do "0I1EPOQN2NLY20435XFKJM4CYOR1LVG2ZXA2ET2TUBS53MFV" end
get '/app/foursquare/redirect_uri' do "https://foursquare.com/settings/connections" end
